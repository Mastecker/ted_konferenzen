# TED Talks

* [Kurs als Ebook](https://mastecker.gitlab.io/ted_konferenzen/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/ted_konferenzen/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/ted_konferenzen/index.html)
