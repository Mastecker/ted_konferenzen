# „TED-Konferenzen“
## …liefern Experten-Impulse in 18 minütigen Videos.


### Was sind TED - Konferenzen?
Die Abkürzung TED steht für Technology, Entertainment, Design. Im Jahr 1984 hat der Architekt Richard Saul Wurman TED ins Leben gerufen, weil er, als Technologie-Entwickler, mit anderen Designern und Entwicklern in Kontakt treten wollte. Die erste TED-Konferenz 1984 war aufgrund finanzieller Mittel kein Erfolg, daher dauerte es sechs Jahre bis 1990 die nächste TED-Konferenz angesetzt wurde. Seitdem gibt es jedes Jahr, seit 2005 auch außerhalb der USA, eine TED-Konferenz (TEDGlobal). Zudem gibt es Ableger der Hauptkonferenz, die weltweit durch unabhängige TED-Konferenzen veranstaltet werden. In den letzten acht Jahren haben in 164 Länder TED-Konferenzen stattgefunden. Auch in Deutschland wurde in Berlin (TEDxBerlin), Stuttgart (TEDxStuttgart) und Hamburg (TEDxHamburg) eine TED-Konferenz abgehalten.
Ursprünglich umfassten die TED-Konferenzen die Themen Technologien, Entertainment und Design, mittlerweile gibt es eine weite Bandbreite von Themen aus den unterschiedlichsten Disziplinen. Dazu gehören u. a. Business, Kultur und Forschungsthemen der Wissenschaft. Das Grundprinzip ist sowohl damals, wie auch heute: „Ideas worth spreading“, also die Verbreitung von Ideen, die es wert sind, um miteinander in einen Austausch zu kommen. Das Ziel ist das Schaffen einer Plattform für das Teilen und Austauschen von Ideen unterschiedlichster Disziplinen. Jeder Vortragende hat nur 18 Minuten Zeit, die Zuhörer für sich zu gewinnen und seine Ideen und Gedanken so ansprechend wie möglich zu präsentieren. Dabei sprechen die Organisationen nicht nur Personen aus der Entwicklung neuer Technologien oder Forscher an, sondern Gemeinschaften und Gruppen aus jedem Bereich, wie z.B. Universitäten, Schulen, Kindergärten, Bibliotheken und Geschäfte und kleinere Unternehmen.
Keiner der Redner erhält ein Honorar für seinen Auftritt oder darf Werbung für eines seiner Werke o. ä. machen. Im Gegenteil, denn die Vortragenden müssen der TED Organisation ihren Beitrag zur Verfügung stellen, damit dieser unter der Creative-Common-Licence veröffentlicht werden kann.
Was sind TED-Talks?
Seit dem Frühjahr 2006 werden die Vorträge auf TED-Konferenzen aufgezeichnet und online auf TED.com für die ganze Welt zur Verfügung gestellt. Mittlerweile gibt es mehr als 2400 Videos. Diese Videoaufnahmen sind dabei nicht länger als die Vorträge, die maximal 18 Minuten andauern dürfen.

### Der Einsatz von TED - Talks in der Bildung
Durch die rasante Verbreitung der TED-Konferenzen und TED-Talks auf der ganzen Welt, wird zunehmend über den Einsatz von TED-Talks in Bildung und Wirtschaft diskutiert. Dazu hat die TED Organisation zunächst selbst den Bereich TEDTalks Education entwickelt, in dem Bildungsfra-gen im Mittelpunkt stehen. Dazu gibt es mittlerweile eine Bibliothek mit einer Reihe von Erklärvideos zu den verschiedensten Themenbereichen. 
Die Videos können nicht nur aus der ganzen Welt angesehen werden, denn die Plattform bietet auch die Möglichkeit über das Video in einen Austausch zu kommen, zu diskutieren oder Fragen zu stellen. Zudem können Quiz- und Lernfragen am Ende des Videos gestellt werden, damit ein noch höherer Lerneffekt erzielt wird. Um den sozialen Charakter mit einfließen zu lassen, gibt es in vielen Städten auch TED Meetups bei denen kollektiv eine Auswahl der Videos gemeinsam geschaut wird und im An-schluss darüber diskutiert wird.

#### Beispiele

TED Ed Lessons
- Biologie
Diversität 
http://ed.ted.com/lessons/why-is-biodiversity-so-important-kim-preshoff

- Umwelt und Nachhaltigkeit 
http://ed.ted.com/lessons/what-really-happens-to-the-plastic-you-throw-away-emma-bryce

- Psychologie
Bilingualität 
http://ed.ted.com/lessons/how-speaking-multiple-languages-benefits-the-brain-mia-nacamulli
Klassische und operante Konditionierung
http://ed.ted.com/lessons/the-difference-between-classical-and-operant-conditioning-peggy-andover
- Bildung
Do schools kill creativity?-Sir Ken Robin-son
https://www.ted.com/talks/ken_robinson_says_schools_kill_creativity



##### Quellen:

- Edelkraut, Franz & Balzer, Stephan (2016). Inspiring! Kommunizieren im TED-Stil. Wiesbaden: Springer Verlag. 
- https://www.ted.com
- https://tedxinnovations.ted.com/2014/11/03/state-of-the-x-tedx-in-october/
- http://www.faz.net/aktuell/wirtschaft/ted-konferenzen-machen-wissenschaft-populaerer-14069016.html


